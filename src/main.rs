// Simple json to csv converter.
// Extract csv data from a json file, where we expect the json to be
// at root level either
// a single object or an array of objects with depth 1.
//

use std::env;
use std::fs::File;
use std::io::{Read, Write};
use std::error::Error;
use getopts::Options;
use serde_json::{Value};

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}


fn get_fields(serde_obj: &Value) -> Result<Vec<String>, Box<dyn Error>> {
    let obj;

    if serde_obj.is_object() {
        obj = serde_obj.as_object().unwrap();
    }
    else if  serde_obj.is_array() {
        obj = serde_obj.as_array()
                       .unwrap()
                       .get(0)
                       .unwrap()
                       .as_object()
                       .unwrap();
    }
    else {
        return Err(From::from("No valid object found"));
    }

    // Collect fields, skipping Objects and Arrays
    let mut fields :Vec<String> = Vec::new();

    for candidate_field in obj.keys() {
        if let Some(c) = obj.get(candidate_field) {

            if c.is_object() || c.is_array() {
                continue
            }

            fields.push(candidate_field.to_string());
        }
    }

    // copy
    Ok(fields)
}


fn dump_data(of: &mut File, delim: &str, fields: &[String]) {
    // write a line to file
    let joined: String = fields.join(delim) + "\n";
    let _ = of.write(joined.as_bytes());
}


fn dump_object(output_file: &mut File, delim: &str, fields: &Vec<String>, serde_map: &serde_json::Map<String, Value>) -> Result<bool, Box<dyn Error>> {
    // Write a set of values to the file, based on the set of fields
    // Emit an error when a field is not available.
    let mut field_values:Vec<String> = Vec::new();

    for f in fields {
        let value: &str = serde_map.get(f)
                                   .unwrap_or_else(|| panic!("Bummer, field is missing in a record: {}", f))
                                   .as_str()                      // as &str gets rid of quotes
                                   .expect("str& cast expected to work here");
        field_values.push(String::from(value)); // cast to regular string, sans the quotes.
    }

    dump_data(output_file, delim, &field_values);
    Ok(true)
}


fn dump_objects(of: &mut File, delim: &str, serde_json: &Value, fields: &Vec<String>, ) -> Result<bool, Box<dyn Error>> {

    if serde_json.is_object() {
        dump_object(of, delim, fields, serde_json.as_object().unwrap())
    }
    else if serde_json.is_array() {
        for obj in serde_json.as_array().expect("Error - array expected") {
            let o = obj.as_object().expect("Error - did not get an object");
            match dump_object(of, delim, fields, o) {
                Err(e) => return Err(e),
                _ => continue
            }
        }

        Ok(true)
    }
    else {
        return Err(From::from("No valid object found"));
    }
}

fn process_json_file(input: String, output: String, use_header: String) -> Result<&'static str, Box<dyn Error>> {
    // Process a json file, getting firstlevel data only
    let mut json_str= String::new();
    File::open(input)?.read_to_string(&mut json_str)?;
    let json_data: Value = serde_json::from_str(json_str.as_str())?;
    // header:
    // we extract all fields from either the first object in Array or from Object itself.
    // expecting that all fields are at least there in the following 2-n objects in case of an Array..
    // TODO: find if we can convert the Object to an Array with one entry to simplify stuff.
    let fields = get_fields(&json_data)?;

    // open file, write stuff.
    let mut of: File  = File::create(output)?;

    if use_header == "1" {
        dump_data(&mut of, ",", &fields);
    }
    dump_objects(&mut of, ",", &json_data, &fields)?;
    Ok("yay")
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let program = &args[0].to_string();
    let mut opts = Options::new();
    opts.optopt("i", "", "set input file name", "NAME");
    opts.optopt("o", "", "set output file name", "NAME");
    opts.optopt("h", "", "infer and write header from key", "NAME");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };

    let input = matches.opt_str("i");
    let output = matches.opt_str("o");
    let use_header = matches.opt_str("h");

    if input.is_none() || output.is_none() || use_header.is_none() {
        print_usage(program, opts);
        std::process::exit(1);
    }

    match process_json_file(input.unwrap(), output.unwrap(), use_header.unwrap()) {
        Err(s) => println!("{}", s),
        Ok(_) => println!("All good!"),
    }
}




#[cfg(test)]
mod tests {
    use serde_json::Value;

    #[test]
    fn test_is_object() {
        let test_json: &str =
            r#"{ "date": "2023-09-04 16:16:07",
                 "sport_name": "FOOTBALL",
                 "sport_id": "17",
                 "country_name": "Portugal",
                 "league_name": "Portugal U23 Taca Revelacao",
                 "id": "922649",
                 "event_dt": "2023-05-23T10:00:00Z",
                 "event_url": "https://sport.toto.nl/wedden/wedstrijd/922649/estoril-praia-vs-benfica",
                 "localteam_name": "Estoril Praia",
                 "visitorteam_name": "Benfica",
                 "home_odds": "2.26",
                 "draw_odds": "3.40",
                 "away_odds": "2.60",
                 "bookie_id": "TO"}"#;

        let data: Value = serde_json::from_str(test_json).unwrap();
        assert_eq!(data.is_object(), true);
    }


    #[test]
    fn test_is_array() {
        let test_json: &str =
            r#"[{ "date": "2023-09-04 16:16:07",
                 "sport_name": "FOOTBALL",
                 "sport_id": "17",
                 "country_name": "Portugal",
                 "league_name": "Portugal U23 Taca Revelacao",
                 "id": "922649",
                 "event_dt": "2023-05-23T10:00:00Z",
                 "event_url": "https://sport.toto.nl/wedden/wedstrijd/922649/estoril-praia-vs-benfica",
                 "localteam_name": "Estoril Praia",
                 "visitorteam_name": "Benfica",
                 "home_odds": "2.26",
                 "draw_odds": "3.40",
                 "away_odds": "2.60",
                 "bookie_id": "TO"},
                {
                   "date": "2023-09-04 16:16:07",
                   "sport_name": "FOOTBALL",
                   "sport_id": "17",
                   "country_name": "Ierland",
                   "league_name": "Ierland Premier Division",
                   "id": "929909",
                   "event_dt": "2023-07-14T17:45:00Z",
                   "event_url": "https://sport.toto.nl/wedden/wedstrijd/929909/ucd-vs-drogheda-united",
                   "localteam_name": "UCD",
                   "visitorteam_name": "Drogheda United",
                   "home_odds": "6.00",
                   "draw_odds": "4.25",
                   "away_odds": "1.48",
                   "bookie_id": "TO"
                 },
                 {
                   "date": "2023-09-04 16:16:07",
                   "sport_name": "FOOTBALL",
                   "sport_id": "17",
                   "country_name": "Zweden",
                   "league_name": "Zweden Superettan",
                   "id": "929916",
                   "event_dt": "2023-07-15T15:00:00Z",
                   "event_url": "https://sport.toto.nl/wedden/wedstrijd/929916/östersunds-vs-landskrona-bois",
                   "localteam_name": "Östersunds",
                   "visitorteam_name": "Landskrona BoIS",
                   "home_odds": "1.78",
                   "draw_odds": "3.70",
                   "away_odds": "4.00",
                   "bookie_id": "TO"
                 }]"#;

        let data: Value = serde_json::from_str(test_json).unwrap();
        assert_eq!(data.is_array(), true);
    }

}
